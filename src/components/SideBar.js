import React from 'react';
import { Link } from 'react-router-dom'
import Condition from './Condition';

function Sidebar(props) {
    return (
        <Condition test={props.visible}>
            <nav id="sidebar" className="sticky">
                <div className="sidebar-header">
                    Civil Cultural
                </div>

                <div className="list-unstyled ml-2 mr-2">
                    <form action="#/" method="post" className="form-inline">
                        <input type="text" name="search" id="search" className="form-control" placeholder="O que você procura?" />
                    </form>
                </div>

                <ul className="list-unstyled components ml-2 mr-2">
                    <li className="active mb-1">
                        <Link to="/">
                            <i className="fas fa-home"></i> Home
                        </Link>
                    </li>
                    <li className="mb-1">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" className="dropdown-toggle"><i className="fas fa-list"></i> Regras</a>
                        <ul className="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="#">Regra 1</a>
                            </li>
                            <li>
                                <a href="#">Regra 2</a>
                            </li>
                            <li>
                                <a href="#">Regra 3</a>
                            </li>
                        </ul>
                    </li>
                    <li className="mb-1">
                            <Link to="/about"><i className="fas fa-briefcase"></i> Mais informações</Link>
                    </li>
                    <li className="mb-1">
                            <Link to="/talk"><i className="fas fa-paper-plane"></i> Chat</Link>
                    </li>
                </ul>
            </nav>
        </Condition>
        
    );
}

export default Sidebar;
